﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace StudentRegistration.Models
{
    public class Student
    {
        [Key]
        public int StudentId { get; set; }


        [Column(TypeName = "nvarchar(100)")]
        [Required]
        [DisplayName("Student Name")]
        public string StudentName { get; set; }


        [Column(TypeName = "nvarchar(100)")]
        [Required]
        [DisplayName("Email Id")]
        public string EmailId { get; set; }


        [Column(TypeName = "nvarchar(50)")]
        [Required]
        [DisplayName("Mobile Numer")]
        public string MobileNumber { get; set; }

        [Column(TypeName = "Date")]
        [DisplayName("Entry Date")]
        [DataType(DataType.Date)]
        public DateTime EntryDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using StudentRegistration.Models;

namespace StudentRegistration.Controllers
{
    public class StudentsController : Controller
    {
        private readonly StudentContext _context;

        public StudentsController(StudentContext context)
        {
            _context = context;
        }

        // GET: Students
        public async Task<IActionResult> Index()
        {
            return View(await _context.tbl_Students.ToListAsync());
        }

        // GET: Students/Create
        public async Task< IActionResult> Create(int id=0)
        {
            if (id == 0)
            {
                return View(new Student());
            }
            else
            {
                return View(await _context.tbl_Students.FirstOrDefaultAsync(x=>x.StudentId==id));
            }
           
        }

        // POST: Students/Create
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StudentId,StudentName,EmailId,MobileNumber,EntryDate")] Student student)
        {
            if (ModelState.IsValid)
            {
                if (student.StudentId==0)
                {
                  await  _context.tbl_Students.AddAsync(student);
                }
                else
                {
                    _context.Update(student);
                }
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

      
        public async Task<IActionResult> Delete(int id)
        {
            var St = await _context.tbl_Students.Where(x => x.StudentId == id).FirstOrDefaultAsync();
            _context.tbl_Students.Remove(St);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
